<?php
//Obserbale

class Observable{

	public $subscriptions = array();

	public function State(Observer $subscription){
		array_push($this->subscriptions,$subscription );
	}


	function update(){
		foreach ($this->subscriptions as $observer) {
			$observer->state();
			echo PHP_EOL;
		}
	}
}