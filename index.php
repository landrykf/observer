<?php
require('./ObserverInterface.php');
require('./Observable.php');
require('./State1.php');
require('./State2.php');


$observable = new Observable();

$newState1 = new State1();

$newState2 = new State2();

// Register to the Observable

$observable->state($newState1);

$observable->state($newState2);
//Send Changement
$observable->update();